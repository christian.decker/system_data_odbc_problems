using System;
using System.IO;
using System.Reflection;
using Interface;

namespace Base
{
    public class EngineeringBaseFactory : IEngineeringBaseFactory
    {
        public IParameter CreateParameter(string name, string value)
        {

            string assemblyPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            FileInfo fileInfo = new FileInfo(assemblyPath);
            if (fileInfo.DirectoryName != null)
            {
                Assembly engineeringBaseAssembly = Assembly.LoadFrom(Path.Combine(fileInfo.DirectoryName, @"..\..\..\..\..\Component\bin\Debug\netstandard2.1\win-x86\Component.dll"));
                Type[] types = engineeringBaseAssembly.GetTypes();
                foreach (Type type in types)
                {
                    if (type.Name == "Class1")
                    {
                        foreach (MethodInfo methodInfo in type.GetMethods())
                        {
                            if (methodInfo.Name == "Test")
                            {
                                ConstructorInfo constructorInfo = type.GetConstructor(Type.EmptyTypes);
                                object obj = constructorInfo.Invoke(new object[] { });
                                methodInfo.Invoke(obj, new object[] { });
                            }
                        }
                    }
                }
            }

            return new Parameter(){
                Name = name,
                Value = value
            };
        }
    }
}