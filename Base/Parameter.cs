using Interface;

namespace Base
{
    public class Parameter : IParameter
    {
        public string Name {get;set;}
        public string Value {get;set;}
    }
}