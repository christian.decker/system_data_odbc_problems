# System.Data.Odbc Problems

This is a repository where I can reproduce an error I have encountered with dotnet core, reflection and the System.Data.Odbc library.

## Steps to reproduce

1. Download git repository on a Windows Operating System with .NET Core 3.1.300 SDK installed
~~~
git clone https://gitlab.com/christian.decker/system_data_odbc_problems.git
cd system_data_odbc_problems
~~~

2. Build application
~~~
dotnet build
~~~

3. Test CLI
~~~
.\CLI\bin\Debug\netcoreapp3.1\win-x86\CLI.exe
~~~

Should display
~~~
Hello World!
System.Data.Odbc.OdbcConnection
~~~

4. Create Pascal wrapper files with Bin64/CNAssemblyImporter.exe from \Interface\bin\Debug\netstandard2.1\win-x86\Interface.dll with C:\Program Files (x86)\dotnet\shared\Microsoft.NETCore.App\3.1.4\netstandard.dll

Error:

![/assets/exception_3.png](/assets/exception_3.png)

4. Create Pascal wrapper files with Bin64/CNAssemblyImporter.exe from \Interface\bin\Debug\netstandard2.1\win-x86\Interface.dll with C:\Windows\Microsoft.NET\Framework\v4.0.30319\netstandard.dll

No error

5. Create same code as in CLI\Program.cs in Delphi and run it
~~~pascal
factory := TEngineeringBaseFactory.Create();
factory.CreateParameter('test', 'test');
~~~


Error:

![/assets/exception_4.png](/assets/exception_4.png)

6. Add System.Data.Odbc manually from C:\Users\<USERNAME\>\.nuget\packages\system.data.odbc\4.7.0\lib\netstandard2.0 to \system_data_odbc_problems\Component\bin\Debug\netstandard2.1\win-x86\

Error:

![/assets/exception_2.png](/assets/exception_2.png)

7. Add System.Data.Odbc manually from C:\Users\<USERNAME\>\.nuget\packages\system.data.odbc\4.7.0\lib\net461 to \system_data_odbc_problems\Component\bin\Debug\netstandard2.1\win-x86\

No error but wrong dll.