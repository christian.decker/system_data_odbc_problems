﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Interface
{
    public class EngineeringBaseFactory : IEngineeringBaseFactory
    {
        private IEngineeringBaseFactory _engineeringBaseFactory;

        internal IEngineeringBaseFactory Factory
        {
            get
            {
                if (_engineeringBaseFactory == null)
                {
                    try
                    {
                        string assemblyPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                        FileInfo fileInfo = new FileInfo(assemblyPath);
                        if (fileInfo.DirectoryName != null)
                        {
                            Assembly engineeringBaseAssembly = Assembly.LoadFrom(Path.Combine(fileInfo.DirectoryName, @"..\..\..\..\..\Base\bin\Debug\netstandard2.1\win-x86\Base.dll"));
                            Type globalType = engineeringBaseAssembly.GetTypes().First(type => type.Name == "Global");
                            if (globalType != null)
                            {
                                FieldInfo engineeringBaseFactoryFieldInfo = globalType.GetField("EngineeringBaseFactory");
                                if (engineeringBaseFactoryFieldInfo.GetValue(null) is IEngineeringBaseFactory engineeringBaseFactory)
                                {
                                    _engineeringBaseFactory = engineeringBaseFactory;
                                }
                            }
                        }
                    }
                    catch (ReflectionTypeLoadException exception)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (Exception loaderException in exception.LoaderExceptions)
                        {
                            stringBuilder.Append(loaderException.Message);
                        }
                        throw new Exception(stringBuilder.ToString());
                    }
                }

                return _engineeringBaseFactory;
            }
        }

        public IParameter CreateParameter(string name, string value)
        {
            return Factory.CreateParameter(name, value);
        }
    }
}
