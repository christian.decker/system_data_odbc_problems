namespace Interface
{
    public interface IParameter
    {
        string Name {get;set;}
        
        string Value {get;set;}
    }
}