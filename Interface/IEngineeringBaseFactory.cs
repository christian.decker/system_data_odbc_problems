﻿namespace Interface
{
    /// <summary>
    /// interface for create engineering base objects
    /// </summary>
    public interface IEngineeringBaseFactory
    {
        /// <summary>
        /// create parameter
        /// </summary>
        /// <param name="name">defines name</param>
        /// <param name="value">defines value</param>
        /// <returns>parameter object</returns>
        IParameter CreateParameter(string name, string value);
    }
}