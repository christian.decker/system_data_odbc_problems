﻿using System;
using Interface;

namespace CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            IEngineeringBaseFactory engineeringBaseFactory = new EngineeringBaseFactory();
            engineeringBaseFactory.CreateParameter("test", "test");
        }
    }
}
